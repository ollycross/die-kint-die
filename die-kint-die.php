<?php

if (class_exists('\Kint')) {
    $aliases = [];

    if (!function_exists('sd') && function_exists('s')) {
        function sd(): void
        {
            if (Kint::$enabled_mode === true) {
                call_user_func_array('s', func_get_args());
                die();
            }
        }
        Kint::$aliases[] = 'sd';
    }

    if (!function_exists('ddd') && function_exists('d')) {
        function ddd(): void
        {
            if (Kint::$enabled_mode === true) {
                call_user_func_array('d', func_get_args());
                die();
            }
        }
        Kint::$aliases[] = 'ddd';
    }
}
