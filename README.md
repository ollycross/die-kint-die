That's German, for "The Kint, the". Adds `sd()` and `ddd()` (debug then die) functions back into Kint-PHP

This package just includes [Kint](https://github.com/kint-php/kint) and adds a couple of convenience functions to die
directly after calling them:

-   `sd(...$args)` - Calls `s(...$args)` and then dies
-   `ddd(...$args)` - Calls `d(...$args)` and then dies

These used to be in Kint and they were useful, this package adds them back in.
